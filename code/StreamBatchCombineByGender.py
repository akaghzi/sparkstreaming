# Append Mode Streaming

import findspark

findspark.init()

from pyspark.sql.types import *
from pyspark.sql import SparkSession

if __name__ == '__main__':
    
    spark = SparkSession.builder.appName('Streaming combines with batch').getOrCreate()
    
    spark.sparkContext.setLogLevel("ERROR")
    
    cSchema = StructType()\
            .add('cid',IntegerType())\
            .add('gender',StringType())\
            .add('age',IntegerType())
                        
    tSchema = StructType()\
            .add('cid',IntegerType())\
            .add('amount',IntegerType())\
            .add('rating',IntegerType())
            
    cDF = spark.read.csv("../dataset/customer.dat",header=False,schema=cSchema)

    tDF = spark\
        .readStream\
        .schema(tSchema)\
        .option('header','false')\
        .option("maxFilesPerExecution",1)\
        .csv('../dataset/transaction')
                    
    print('')
    print('Customer schema')
    print(cDF.printSchema())

    print('')
    print('Is streaming ready?')
    print(tDF.isStreaming)
    
    print('')
    print('Streading data schema')
    print(tDF.printSchema())
    
    combineDF = cDF.join(tDF,['cid']).select(cDF.gender,tDF.amount).groupBy("gender").agg({"amount":"sum"})

    query = combineDF\
            .writeStream\
            .outputMode('complete')\
            .format('console')\
            .option('truncate','false')\
            .option('numRows',30)\
            .start()\
            .awaitTermination()
            
            
    