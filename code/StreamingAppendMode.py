# Append Mode Streaming

import findspark

findspark.init()

from pyspark.sql.types import *
from pyspark.sql import SparkSession

if __name__ == '__main__':
    
    spark = SparkSession.builder.appName('Streaming Append Mode').getOrCreate()
    
    spark.sparkContext.setLogLevel("ERROR")
    
    schema = StructType()\
            .add('lsoa_code',StringType())\
            .add('borough',StringType())\
            .add('major_category',StringType())\
            .add('minor_category',StringType())\
            .add('value',StringType())\
            .add('year',StringType())\
            .add('month',StringType())
                        
    fileStreamDF = spark\
        .readStream\
        .schema(schema)\
        .option('header','true')\
        .csv('../dataset/droplocation')
                    
    print('')
    print('Is streaming ready?')
    print(fileStreamDF.isStreaming)
    
    print('')
    print('Streading data schema')
    print(fileStreamDF.printSchema())
    
    trimmedDF = fileStreamDF\
                .select(\
                'borough',\
                'year',\
                'month',\
                'value')\
                .withColumnRenamed(\
                'value',\
                'convictions')
                
    query = trimmedDF\
            .writeStream\
            .outputMode('append')\
            .format('console')\
            .option('truncate','false')\
            .option('numRows',30)\
            .start()\
            .awaitTermination()
            
            
    