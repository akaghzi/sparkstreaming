import sys
from afinn import Afinn
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf,explode,split
from pyspark.sql.types import StringType,FloatType

if __name__=="__main__":
    spark=SparkSession.builder.appName("Tweet Sentiment").master("local").getOrCreate()
    if len(sys.argv) != 4:
        print("Usage: spark-submit sparkTweetSentiment.py <host> <port> <topic>",file=sys.stderr)
    
    host=sys.argv[1]
    port=sys.argv[2]
    topic=sys.argv[3]
    
    spark.sparkContext.setLogLevel("ERROR")
    
    tweetDFRaw = spark\
            .readStream\
            .format("kafka")\
            .option("kafka.bootstrap.servers","localhost:9092")\
            .option("subscribe",topic)\
            .load()
            
    tweetDF = tweetDFRaw.selectExpr("CAST(value AS STRING) as tweet")
    
    tweetDF.printSchema()
    
    afinn = Afinn()
    
    def addSentimentScore(tweet):
        return afinn.score(tweet)
    
    sentimentScore = udf(addSentimentScore, FloatType())
    
    resultDF = tweetDF.withColumn("sentiment_score", sentimentScore(tweetDF.tweet))

    query = resultDF\
            .writeStream\
            .outputMode("append")\
            .format("console")\
            .option("truncate",False)\
            .trigger(processingTime="5 seconds")\
            .start()\
            .awaitTermination()