# Consumer API keys
# HHFE4LUQuMWTp2qtgBr8zDR8s (API key)
#
# 7VL4UQIU7nj2AWYGqF9ebKSxTVJcEOzAP5CrM6FvDlZwNvcprH (API secret key)
# Access token & access token secret
# 33998075-ZsrmL8v32ecPmmkdlU1m0iVxpMmyZ9TAXTo53D0UF (Access token)
#
# yEow9oPCapNbgj7fRFR4Acap53IEclnhKgClHyZtOlbZt (Access token secret)


import sys, os, json, tweepy, socket

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

class TweetListener(StreamListener):
    def __init__(self,socket):
        print("listener")
        self.client_socket = socket
    
    def on_data(self,data):
        try:
            jsonData = json.load(data)
            message = jsonmessage["text"].encode("utf-8")
            print(message)
            self.client_socket.send(message)
            
        except BaseException as e:
            print("Error: ", str(e))    
            
        return True
    
    def on_error(self, status):
        
        print(status)
        return True
        
def connect_to_twitter(connection,tracks):
    api_key = "HHFE4LUQuMWTp2qtgBr8zDR8s"
    api_secret = "7VL4UQIU7nj2AWYGqF9ebKSxTVJcEOzAP5CrM6FvDlZwNvcprH"
    
    access_token = "33998075-ZsrmL8v32ecPmmkdlU1m0iVxpMmyZ9TAXTo53D0UF"
    access_token_secret = "yEow9oPCapNbgj7fRFR4Acap53IEclnhKgClHyZtOlbZt"
    
    auth = OAuthHandler(api_key, api_secret)
    auth.set_access_token(access_token, access_token_secret)
    
    twitter_stream = Stream(auth, TweetListener(connection))
    twitter_stream.filter(track=tracks, languages="en")
        
        
if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python TwitterStreaming.py <hostname> <port> tracks")
        exit(-1)

    host=sys.argv[1]
    port=int(sys.argv[2])
    tracks=sys.argv[3:]

    s = socket.socket()
    s.bind((host,port))

    print("listener on port", port)

    s.listen(5)

    connection, client_address = s.accept()

    print(str(client_address))
    print(tracks)

    connect_to_twitter(connection,tracks)