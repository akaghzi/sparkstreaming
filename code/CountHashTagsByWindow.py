import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf,explode,split,window
from pyspark.sql.types import StringType

if __name__=="__main__":
    spark=SparkSession.builder.appName("Count Hash Tags").getOrCreate()
    if len(sys.argv) != 3:
        print("Usage: spark-submit CountHashTags.py <host> <port>",file=sys.stderr)
    
    host=sys.argv[1]
    port=sys.argv[2]
    
    spark.sparkContext.setLogLevel("ERROR")
    
    lines = spark\
            .readStream\
            .format("socket")\
            .option("includeTimestamp",True)\
            .option("host",host)\
            .option("port",port)\
            .load()
            
    words = lines.select(explode(split(lines.value," ")).alias("word"),lines.timestamp)
    
    def extractTags(word):
        if word.lower().startsWith("#"):
            return word
        else:
            return "NonTag"
    
    extract_tags_udf = udf(extractTags, StringType())
    resultDF = words.withColumn("tags",extract_tags_udf(words.word))
    
    hashTagsCount = resultDF.where(resultDF.tags != "NonTag")\
                            .groupBy(window(resultDF.timestamp,"50 seconds","30 seconds"),resultDF.tags)\
                            .count()\
                            .orderBy("count",ascending=False)
    
    query = hashTagsCount.writeStream.outputMode("complete").format("console").option("truncate",False).start().awaitTermination()