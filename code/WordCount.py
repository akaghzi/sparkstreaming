import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, split

if __name__ == '__main__':

    if len(sys.argv) != 3:
        print("Usage: spark-submit requires WordCount.py file, <hostname> <port>")
        exit(-1)

    host=sys.argv[1]
    port=int(sys.argv[2])

    spark = SparkSession\
        .builder\
        .appName("Word Count Application")\
        .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    lines = spark\
        .readStream\
        .format('socket')\
        .option('host',host)\
        .option('port',port)\
        .load()

    words = lines.select(explode(split(lines.value,' ')).alias('word'))

    words.printSchema()

    wordCount = words.groupBy('word').count()

    query = wordCount.writeStream.outputMode('complete').format('console').start()

    query.awaitTermination()