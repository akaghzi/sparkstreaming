import sys, os, json, tweepy, socket, pykafka

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

class TweetListener(StreamListener):
    def __init__(self,kafkaProducer):
        print("listener")
        self.producer = kafkaProducer
    
    def on_data(self,data):
        try:
            jsonData = json.load(data)
            words = jsonData["text"].split()
            # print(message)
            hashTagList = list(filter(lambda x: x.lower().startsWith("#"),words))
            if len(hashTagList!=0):
                for hashTag in hashTagList:
                    print(hashTag)
                    self.producer.produce(bytes(hashTag,"utf-8"))
            
        except KeyError as e:
            print("Error: %s" % str(e))    
            
        return True
    
    def on_error(self, status):
        
        print(status)
        return True
    
def connect_to_twitter(kafkaProducer,tracks):
    api_key = "HHFE4LUQuMWTp2qtgBr8zDR8s"
    api_secret = "7VL4UQIU7nj2AWYGqF9ebKSxTVJcEOzAP5CrM6FvDlZwNvcprH"
    
    access_token = "33998075-ZsrmL8v32ecPmmkdlU1m0iVxpMmyZ9TAXTo53D0UF"
    access_token_secret = "yEow9oPCapNbgj7fRFR4Acap53IEclnhKgClHyZtOlbZt"
    
    auth = OAuthHandler(api_key, api_secret)
    auth.set_access_token(access_token, access_token_secret)
    
    twitter_stream = Stream(auth, TweetListener(kafkaProducer))
    twitter_stream.filter(track=tracks, languages="en")

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python kafkaHashTagProducer.py <hostname> <port> tracks")
        exit(-1)

    host=sys.argv[1]
    port=int(sys.argv[2])
    topic=sys.argv[3]
    tracks=sys.argv[4:]

    kafkaClient = pykafka.KafkaClient(host+":"+str(port))
    kafkaProducer = kafkaClient.topics[bytes(topic,"utf-8")].get_producer()
    
    connect_to_twitter(kafkaProducer,tracks)
    